##################### Data Clearning ###############

library(dplyr)

### 1. Normal distributed response variable

# read in the data for player values 
pv_1718 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/PlayerValues/transfermarkt_fbref_201718.csv", sep = ";")
pv_1819 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/PlayerValues/transfermarkt_fbref_201819.csv", sep = ";")
pv_1920 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/PlayerValues/transfermarkt_fbref_201920.csv", sep = ";")
# pool the data of three years together
pv <- rbind(pv_1718, pv_1819, pv_1920)
# create new variable for log(value)
pv$log_value <- log(pv$value)
# separate the data by position of the players, because the value of players of different positions are affected by different factors. Attention: some players serve multiple positions. 
pv_GK <- pv %>% filter(grepl("GK", position)) # for goal keeper
pv_DF <- pv %>% filter(grepl("DF", position)) # for defender
pv_MF <- pv %>% filter(grepl("MF", position)) # for middle fielder
pv_FW <- pv %>% filter(grepl("FW", position)) # for forwards
# omit missing values
pv_GK <- na.omit(pv_GK)
pv_DF <- na.omit(pv_DF)
pv_MF <- na.omit(pv_MF)
pv_FW <- na.omit(pv_FW)
# delete the outlier with age zero
pv_GK <- pv_GK %>% filter(age!=0)
pv_DF <- pv_DF %>% filter(age!=0)
pv_MF <- pv_MF %>% filter(age!=0)
pv_FW <- pv_FW %>% filter(age!=0)
# select relevant variales
pv_GK <- pv_GK %>% select(log_value,age,GDiff,wins_gk,clean_sheets,GA)
pv_DF <- pv_DF %>% select(log_value,age,GDiff,pass_targets,carries,GA)
pv_MF <- pv_MF %>% select(log_value,age,GDiff,assists,pass_targets,carries)
pv_FW <- pv_FW %>% select(log_value,age,GDiff,goals,shots_on_target,touches_att_pen_area)
# export the data
write.csv(pv_GK, "pv_GK.csv")
write.csv(pv_DF, "pv_DF.csv")
write.csv(pv_MF, "pv_MF.csv")
write.csv(pv_FW, "pv_FW.csv")

### 2. Beta distributed response variable

## Transform position into numerical numbers
# FW <- 1, MF <- 2, DF <- 3, GK <- 4
# take avg if someone take multiple positions
# write a function to assign number to single position
pos <- function(position){
  dictionary <- c("FW", "MF", "DF", "GK")
  return(which(dictionary==position))
}
# use this function to transform position into numeric numbers
pv$position_num <- NA
for (i in 1:nrow(pv)) {
  # single position
  if (nchar(pv$position[i])==2) {
    pv$position_num[i] <- pos(pv$position[i])
  }
  # multiple positions
  if (nchar(pv$position[i])>2) {
    positions_i <- strsplit(pv$position[i], split = ",")[[1]]
    positions_num_i <- sapply(positions_i, pos)
    pv$position_num[i] <- mean(positions_num_i)
  }  
}

# transform passes_pct from percentage point to (0,1)
pv$passes_pct_float <- as.numeric(pv$passes_pct)/100

# get the data set with relevant variables
pv_pass <- pv %>% select(passes_pct_float,position_num,passes_ground,passes_short,crosses, GDiff)
# remove NAs
pv_pass<- na.omit(pv_pass)

# remove o and 1, since these are very rare cases in reality
pv_pass <- pv_pass %>% filter(passes_pct_float!=0 & passes_pct_float!=1)

# export the data
write.csv(pv_pass, "pv_pass.csv")

### 3. Negative binomial distributed response variable

# read in the match-level data for top 5 leagues
DE_1415 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2014-2015.csv")
DE_1516 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2015-2016.csv")
DE_1617 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2016-2017.csv")
DE_1718 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2017-2018.csv")
DE_1819 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2018-2019.csv")
DE_1920 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2019-2020.csv")
DE_2021 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2020-2021.csv")
DE_2122 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/bundesliga/2021-2022.csv")
UK_1415 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2014-2015.csv")
UK_1516 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2015-2016.csv")
UK_1617 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2016-2017.csv")
UK_1718 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2017-2018.csv")
UK_1819 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2018-2019.csv")
UK_1920 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2019-2020.csv")
UK_2021 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2020-2021.csv")
UK_2122 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/epl/2021-2022.csv")
ES_1415 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2014-2015.csv")
ES_1516 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2015-2016.csv")
ES_1617 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2016-2017.csv")
ES_1718 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2017-2018.csv")
ES_1819 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2018-2019.csv")
ES_1920 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2019-2020.csv")
ES_2021 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2020-2021.csv")
ES_2122 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/la_liga/2021-2022.csv")
FR_1415 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2014-2015.csv")
FR_1516 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2015-2016.csv")
FR_1617 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2016-2017.csv")
FR_1718 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2017-2018.csv")
FR_1819 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2018-2019.csv")
FR_1920 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2019-2020.csv")
FR_2021 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2020-2021.csv")
FR_2122 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/ligue_1/2021-2022.csv")
IT_1415 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2014-2015.csv")
IT_1516 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2015-2016.csv")
IT_1617 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2016-2017.csv")
IT_1718 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2017-2018.csv")
IT_1819 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2018-2019.csv")
IT_1920 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2019-2020.csv")
IT_2021 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2020-2021.csv")
IT_2122 <- read.csv("C:/Users/wuqio/Dropbox/current_topics_in_applied_statistics/Qiong/Top5Leagues/serie_a/2021-2022.csv")

# add the information of country
DE_1415$country <- "DE"
DE_1516$country <- "DE"
DE_1617$country <- "DE"
DE_1718$country <- "DE"
DE_1819$country <- "DE"
DE_1920$country <- "DE"
DE_2021$country <- "DE"
DE_2122$country <- "DE"
UK_1415$country <- "UK"
UK_1516$country <- "UK"
UK_1617$country <- "UK"
UK_1718$country <- "UK"
UK_1819$country <- "UK"
UK_1920$country <- "UK"
UK_2021$country <- "UK"
UK_2122$country <- "UK"
ES_1415$country <- "ES"
ES_1516$country <- "ES"
ES_1617$country <- "ES"
ES_1718$country <- "ES"
ES_1819$country <- "ES"
ES_1920$country <- "ES"
ES_2021$country <- "ES"
ES_2122$country <- "ES"
FR_1415$country <- "FR"
FR_1516$country <- "FR"
FR_1617$country <- "FR"
FR_1718$country <- "FR"
FR_1819$country <- "FR"
FR_1920$country <- "FR"
FR_2021$country <- "FR"
FR_2122$country <- "FR"
IT_1415$country <- "IT"
IT_1516$country <- "IT"
IT_1617$country <- "IT"
IT_1718$country <- "IT"
IT_1819$country <- "IT"
IT_1920$country <- "IT"
IT_2021$country <- "IT"
IT_2122$country <- "IT"

# pool data together over all years and countries
matches <- rbind(DE_1415,DE_1516,DE_1617,DE_1718,DE_1819,DE_1920,DE_2021,DE_2122,
                 UK_1415,UK_1516,UK_1617,UK_1718,UK_1819,UK_1920,UK_2021,UK_2122,
                 ES_1415,ES_1516,ES_1617,ES_1718,ES_1819,ES_1920,ES_2021,ES_2122,
                 FR_1415,FR_1516,FR_1617,FR_1718,FR_1819,FR_1920,FR_2021,FR_2122,
                 IT_1415,IT_1516,IT_1617,IT_1718,IT_1819,IT_1920,IT_2021,IT_2122)

# combine betting odds from different providers
matches$avg_odds_HW <- (matches$B365H+matches$BWH+matches$IWH+matches$PSH+matches$WHH+matches$VCH+matches$PSCH)/7
# create covariates
matches$DIFF_OA <- matches$HTOa-matches$ATOa
matches$DIFF_HTAT_ATDEF <- matches$HTAt-matches$HTDef
matches$DIFF_MV <- matches$HomeMV-matches$AwayMV
# get all only the relevant variables into one dataset
goals <- matches %>% select(FTHG, DIFF_OA, DIFF_HTAT_ATDEF, DIFF_MV, HxG, avg_odds_HW)
# omit missing values
goals <- na.omit(goals)

# export the data
write.csv(goals, "goals.csv")
