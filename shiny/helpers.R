library(shiny)
library(gamlss)
library(DT)
library(shinydisconnect)
library(shinyBS)
library(caret)
library(dplyr)
library(shinycssloaders)
library(shinyjs)
library(tableHTML)
library(table1)
library(htmlTable)
#library(latex2exp)
options(spinner.color="#153268", spinner.color.background="#ffffff", spinner.size=2)

pv_GK <- read.csv("pv_GK.csv")
pv_DF <- read.csv("pv_DF.csv")
pv_MF <- read.csv("pv_MF.csv")
pv_FW <- read.csv("pv_FW.csv")
pv_pass <- read.csv("pv_pass.csv")
goals <- read.csv("goals.csv")
# For all datasets, first column is observation index, second column is y, 3-7 columns are covariates.

# define function to plot fit for univariate model
# function for normally distributed rv
uni.fit.normal <- function(x, y, fitted_model, xlab="covariate", ylab = "response"){
  yhat <- predict(fitted_model, what = "mu", type = "response", se.fit = T)
  sighat <- predict(fitted_model, what = "sigma", type = "response")
  # se.pi <- sqrt(yhat$se.fit^2 + sighat^2) # se for prediction interval
  se.pi <- sighat
  
  # information used for plot
  data.plot <- data.frame(x = x,
                          y = y,
                          yhat = yhat$fit,
                          pi_low = yhat$fit - 1.96*se.pi,
                          pi_high = yhat$fit + 1.96*se.pi)
  data.plot <- data.plot[order(data.plot$x), ]
  
  p <- ggplot(data.plot, aes(x = x, y = y)) + geom_point() +
        geom_line(aes(x, yhat)) +
        geom_ribbon( aes(ymin = pi_low, ymax = pi_high), fill = "green", alpha = 0.2 ) +
        xlab(xlab) +
        ylab(ylab)
  p
}

# function for beta distributed rv
uni.fit.beta <- function(x, y, fitted_model, xlab="covariate", ylab = "response"){
  muhat <- predict(fitted_model, what = "mu", type = "response")
  sighat <- predict(fitted_model, what = "sigma", type = "response")
  
  # re-parametrization
  repara <- function(mu, sig2){
    alpha <- mu * (1/sig2 - 1)
    beta <- 1/sig2 - alpha - 1
    c(alpha, beta)
  }
  
  beta.para <- apply(cbind(muhat, sighat^2), 1, function(p){ repara(p[1], p[2]) })
  
  # quantiles for beta distribution as prediction interval limits
  pi.limit <- apply(beta.para, 2, function(p){ qbeta(c(0.025, 0.975), shape1=p[1], shape2=p[2]) })

  # information used in plot
  data.plot <- data.frame(x = x,
                          y = y,
                          yhat = muhat,
                          pi_low = pi.limit[1, ],
                          pi_high = pi.limit[2, ])
  data.plot <- data.plot[order(data.plot$x), ]
  
  p <- ggplot(data.plot, aes(x = x, y = y)) + geom_point() +
    geom_line(aes(x, yhat)) +
    geom_ribbon( aes(ymin = pi_low, ymax = pi_high), fill = "green", alpha = 0.2 ) +
    xlab(xlab) +
    ylab(ylab)
  p
}

# function for negative binomial distributed rv
uni.fit.negbinom <- function(x, y, fitted_model, xlab="covariate", ylab = "response"){
  muhat <- predict(fitted_model, what = "mu", type = "response")
  sighat <- predict(fitted_model, what = "sigma", type = "response")
  
  # re-parametrization
  negbin.para <- cbind(muhat, 1/sighat)
  
  # quantiles for beta distribution as prediction interval limits
  pi.limit <- apply(negbin.para, 1, function(p){ qnbinom(c(0.025, 0.975), size = p[2], mu=p[1]) })
  
  # information used in plot
  data.plot <- data.frame(x = x,
                          y = y,
                          yhat = muhat,
                          pi_low = pi.limit[1, ],
                          pi_high = pi.limit[2, ])
  data.plot <- data.plot[order(data.plot$x), ]
  
  p <- ggplot(data.plot, aes(x = x, y = y)) + geom_point() +
    geom_line(aes(x, yhat)) +
    geom_ribbon( aes(ymin = pi_low, ymax = pi_high), fill = "green", alpha = 0.2 ) +
    xlab(xlab) +
    ylab(ylab)
  p
}

# define function to plot fit for multivariate model
# multi.fit <- function(data_train, data_test, fitted_model){
#   # predict yhat for training set
#   yhat_multi_train <- predict(
#     fitted_model,
#     what = "mu",
#     type = "response",
#     newdata = data_train)
# 
#   # predict yhat for testing set
#   yhat_multi_test <- predict(
#     fitted_model,
#     what = "mu",
#     type = "response",
#     newdata = data_test)
# 
#   # plot y against yhat for training set and testing set
#   par(mfrow = c(1, 2))
# 
#   plot(y=data_train[,2], x=yhat_multi_train,
#        # xlim = range(yhat_multi_train), ylim = range(data_train[,2]),
#        xlab = "y_hat", ylab = "y", main = "Model fit_training")
#   abline(coef = c(0, 1), col="red")
# 
#   plot(y=data_test[,2], x=yhat_multi_test,
#        # xlim = range(yhat_multi_test), ylim = range(data_test[,2]),
#        xlab = "y_hat", ylab = "y", main = "Model fit_testing")
#   abline(0,1, col="red")
# 
#   p <- recordPlot()
#   p
# }

uni.fit.comp <- function(x, y, model1, model2, model3, xlab="covariate", ylab="response variable"){
  # model1 must be GLM, model2 must be GAM, model3 must be GAMLSS
  muhat1 <- predict(model1, what = "mu", type = "response")
  muhat2 <- predict(model2, what = "mu", type = "response")
  muhat3 <- predict(model3, what = "mu", type = "response")
  data.plot <- cbind(x, y, muhat1, muhat2, muhat3)
  data.plot <- data.plot[order(x), ]
  plot(x=data.plot[, 1], y=data.plot[, 2], xlab=xlab, ylab=ylab)
  lines(x=data.plot[, 1], y=data.plot[, 3], col=2, lwd=2)
  lines(x=data.plot[, 1], y=data.plot[, 4], col=3, lwd=2)
  lines(x=data.plot[, 1], y=data.plot[, 5], col=4, lwd=2)
  legend("bottomleft", col = 2:4, lty=rep(1,3), legend = c("GLM", "GAM", "GAMLSS"), bty = "n")
}

uni.fit.qqplot <- function(model1, model2, model3){
  q1 <- Reduce(cbind, qqnorm(resid(model1)))
  q2 <- Reduce(cbind, qqnorm(resid(model2)))
  q3 <- Reduce(cbind, qqnorm(resid(model3)))
  
  data.plot <- merge(q1, q2, by = "init")
  data.plot <- merge(data.plot, q3, by = "init")
  
  plot(data.plot[, 1], data.plot[, 2], col=2,
       xlab = "Theoretical Quantiles",
       ylab = "Sample Quantiles")
  points(data.plot[, 1], data.plot[, 3], col=3)
  points(data.plot[, 1], data.plot[, 4], col=4)
  abline(a=0, b=1, col = "black", lwd = 2)
  legend("topleft", col = 2:4, pch = rep(1,3), legend = c("GLM", "GAM", "GAMLSS"), bty = "n")

  p <- recordPlot()
  p
}

fit.qqplot.single <- function(model){
  qqnorm(resid(model))
  qqline(resid(model), col = "red", lwd = 2)
  
  p <- recordPlot()
  p
}

multi.fit <- function(data_train, fitted_model, main=""){
  # predict yhat for training set
  yhat_multi_train <- predict(fitted_model)
  
  plot(y=data_train[,2], x=yhat_multi_train,
       xlim = range(c(yhat_multi_train, data_train[,2])),
       ylim = range(c(yhat_multi_train, data_train[,2])),
       xlab = expression(hat(y)), ylab = "y", main=main)
  abline(coef = c(0, 1), col="red")
  
  p <- recordPlot()
  p
}


# define function to split data into training and testing set
datasplit <- function(data_full, train_prop=0.5, seed=123){
  set.seed(seed = seed)
  inTrain <- createDataPartition(
    y = data_full[,2], # the outcome data are needed
    times = 1, # number of split
    p = train_prop, # the percentage of data in the training set
    list = FALSE # return a vector of T or F, indicating whether certain row belongs to the training set.
    # if list = TRUE, return a list instead of vector.
  )
  # The output is a set of integers for the rows that belong in the training set.
  
  # split into training set and testing set
  return(list(train=data_full[inTrain,], test=data_full[-inTrain,]))

}

# For several models, it takes long time to fit the model. We save the 
# fitting results beforehand and load results here for fast processing.
load(file = "./gamlss_multi_beta_nonlinear.RData")
load(file = "./gamlss_multi_beta_linear_nonlinear.RData")
load(file = "./gamlss_multi_beta_all.RData")
load(file = "./gamlss_multi_nb_nonlinear.RData")
load(file = "./gamlss_multi_nb_linear_nonlinear.RData")
load(file = "./gamlss_multi_nb_all.RData")
load(file = "./gam_multi_beta.RData")
load(file = "./gam_multi_nb.RData")
load(file = "./gamlss_multi_nb_linear_interaction.RData")
load(file = "./gam_uni_nb_diffmv.RData")
load(file = "./gamlss_uni_nb_diffmv_pb.RData")