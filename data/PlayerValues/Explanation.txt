Merged data from transfermarkt.de and fbref.com regarding soccer players from top 5 European leagues, used for my bachelor's thesis "Modelling Football Players Values and Their Determinants on a Transfer Market using Robust Regression Models'. More details on:
https://github.com/RSKriegs/Modelling-Football-Players-Values-on-Transfer-Market-and-Their-Determinants-using-Robust-Regression


# https://www.kaggle.com/datasets/kriegsmaschine/soccer-players-values-and-their-statistics?select=transfermarkt_fbref_201819.csv
